This diary file is written by Howard Huang (N18044010) in the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-05
* Due to the COVID-19 epidemic, professor is working on converting this course to an online course for everyone's safety.

# 2020-03-12
* Professor provides a short welcome message on YouTube: https://youtu.be/-XyF-sJYiDQ.
* Watch video: Success stories where deep neural networks outperform humans presentation video.
* Watch video: Introduction of the digit recognition case study.
* Sign up to participate in the course and get access to the material by registering in Google Sheet.
* Fill in the survey about your background and expectations in Google sheet.